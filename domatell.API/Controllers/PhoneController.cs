﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domatel.Interface.Interfaces.Service;
using domatel.Models.Core;

using domatel.Models.Criteria.Phone;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace domatell.API.Controllers
{
    [Route("api/[controller]")]
  
    [ApiController]
    public class PhoneController : ControllerBase
    {
        private readonly IPhoneService _phoneService;

        public PhoneController(IPhoneService phoneService)
        {
            _phoneService = phoneService;
        }

        [HttpGet("GetAllPhone")]
        public async Task<ServiceResult<string>> GetAllPhone()
        {
            return await _phoneService.GetAllPhone();
        }

        [HttpGet("GetPhoneInfoById")]
        public async Task<ServiceResult<string>> GetPhoneInfoById(int id)
        {
            return await _phoneService.GetPhoneInfoById(id);
        }

        [HttpGet("GetOwnerPhoneById")]
        public async Task<ServiceResult<string>> GetOwnerPhoneById(int id)
        {
            return await _phoneService.GetOwnerPhoneById(id);
        }

        [HttpGet("GetAllPhoneById")]
        public async Task<ServiceResult<string>> GetAllPhoneById(int id)
        {
            return await _phoneService.GetAllPhoneById(id);
        }

        [HttpPost]
        [Route("PostPhone")]
        public async Task<ServiceResult> PostPhone(PhoneAdd model)
        {
            return await _phoneService.AddPhone(model);
        }
    }
}