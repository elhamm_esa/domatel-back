﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domatel.Interface;
using domatel.Models.Core;
using domatel.Models.Users;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace domatell.API.Controllers
{
    [Route("api/[controller]")]
    
 
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
       

        [HttpPut("PutUser")]
        public async Task<ServiceResult> PutUser(User model)
        {
            return await _userService.EditUser(model);
        }

        [HttpPost]
        public async Task<ServiceResult> PostUser(User model)
        {
            return await _userService.AddUser(model);
        }

        [HttpGet("GetUserById/{id}")]
        public async Task<ServiceResult<string>> GetUserById(string userId)
        {
            return await _userService.GetUserById(userId);
        }

        [HttpGet("GetDomain/{id}")]
        public async Task<ServiceResult<string>> GetDomain(string userId)
        {
            return await _userService.Domain(userId);
        }

        [HttpGet("GetMySimCart/{id}")]
        public async Task<ServiceResult<string>> GetSimCart(string userId)
        {
            return await _userService.SimCart(userId);
        }

        [HttpGet("GetMyPhone/{id}")]
        public async Task<ServiceResult<string>> GetPhone(string userId)
        {
            return await _userService.Phone(userId);
        }

        [HttpGet("GetBidSimCart/{id}")]
        public async Task<ServiceResult<string>> GetBidSimCart(string userId)
        {
            return await _userService.BidSimCart(userId);
        }

        [HttpGet("GetBidDomain/{id}")]
        public async Task<ServiceResult<string>> GetBidDomain(string userId)
        {
            return await _userService.BidDomain(userId);
        }

        [HttpGet("GetBidPhone/{id}")]
        public async Task<ServiceResult<string>> GetBidPhone(string userId)
        {
            return await _userService.BidPhone(userId);
        }
    }
}