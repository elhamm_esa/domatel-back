﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using domatel.DataLayer.Data;
using domatel.Interface.Interfaces.Repository;
using domatel.Interface.Interfaces.Service;
using domatel.Models;
using domatel.Models.Bids;
using domatel.Models.Core;
using domatel.Services.Utility;
using Microsoft.EntityFrameworkCore;

namespace domatel.Services.Repository
{
  public  class BidRepository:IBidRepository
    {
        private DomatelContext _domatelContext;
        public BidRepository(DomatelContext domatelContext)

        {
            _domatelContext = domatelContext;
        }
        

        public async Task<ServiceResult> AddBid(Bid model)
        {
            try
            {
                await _domatelContext.Bids.AddAsync(model);
                await _domatelContext.SaveChangesAsync();
                return new ServiceResult
                {
                    Message = string.Empty,
                    Status = (int)Configuration.ServiceResultStatus.Success
                };
            }
            catch (Exception e)
            {
                return new ServiceResult
                {
                    Message = e.Message,
                    Status = (int)Configuration.ServiceResultStatus.Error
                };
            }
        }
    }
}
